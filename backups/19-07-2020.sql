CREATE SCHEMA tesisvalderrama;

CREATE TABLE tesisvalderrama.cargo ( 
	codigo               bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	descripcion          varchar(30)  NOT NULL    ,
	estado               char(1)  NOT NULL DEFAULT 'A'   
 );

CREATE TABLE tesisvalderrama.contacto ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	nombres              varchar(255)      ,
	apellidos            varchar(255)      ,
	telefono             varchar(11)      ,
	email                varchar(255)      ,
	asunto               varchar(255)      ,
	mensaje              longtext      ,
	estado               char(1)   DEFAULT 'A'   
 );

CREATE TABLE tesisvalderrama.galeria_tipo ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	nombre               varchar(255)      ,
	estado               char(1)   DEFAULT 'A'   
 );

CREATE TABLE tesisvalderrama.menu ( 
	codigo_menu          bigint  NOT NULL    PRIMARY KEY,
	icono                varchar(30)   DEFAULT 'fa-circle-o'   ,
	nombre               varchar(50)  NOT NULL    ,
	descripcion          text  NOT NULL    ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	nivel                int  NOT NULL DEFAULT 1   ,
	orden                int  NOT NULL DEFAULT 0   ,
	archivo              varchar(50)  NOT NULL    ,
	id_padre             bigint      ,
	CONSTRAINT menu_ibfk_1 FOREIGN KEY ( id_padre ) REFERENCES tesisvalderrama.menu( codigo_menu ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX id_padre ON tesisvalderrama.menu ( id_padre );

CREATE TABLE tesisvalderrama.menu_accesos ( 
	menu                 bigint  NOT NULL    ,
	cargo                bigint  NOT NULL    ,
	acceso               char(1)  NOT NULL DEFAULT '1'   ,
	agregar              char(1)  NOT NULL DEFAULT '1'   ,
	modificar            char(1)  NOT NULL DEFAULT '1'   ,
	eliminar             char(1)  NOT NULL DEFAULT '1'   ,
	imprimir             char(1)  NOT NULL DEFAULT '1'   ,
	CONSTRAINT pk_menu_accesos PRIMARY KEY ( menu, cargo ),
	CONSTRAINT menu_accesos_ibfk_3 FOREIGN KEY ( cargo ) REFERENCES tesisvalderrama.cargo( codigo ) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT menu_accesos_ibfk_2 FOREIGN KEY ( menu ) REFERENCES tesisvalderrama.menu( codigo_menu ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX menu ON tesisvalderrama.menu_accesos ( menu );

CREATE INDEX rol ON tesisvalderrama.menu_accesos ( cargo );

CREATE TABLE tesisvalderrama.objeto_tipo ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	nombre               varchar(255)      ,
	descripcion          text      ,
	estado               char(1)   DEFAULT 'A'   
 );

CREATE TABLE tesisvalderrama.persona ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	dni                  char(8)  NOT NULL    ,
	nombres              varchar(50)  NOT NULL    ,
	apellido_paterno     varchar(20)  NOT NULL    ,
	apellido_materno     varchar(20)  NOT NULL    ,
	sexo                 tinyint  NOT NULL    ,
	telefono             char(11)      ,
	email                varchar(50)  NOT NULL    ,
	cargo                bigint      ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	CONSTRAINT email UNIQUE ( email ) ,
	CONSTRAINT persona_ibfk_1 FOREIGN KEY ( cargo ) REFERENCES tesisvalderrama.cargo( codigo ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX cargo ON tesisvalderrama.persona ( cargo );

CREATE TABLE tesisvalderrama.simulador ( 
	id_simulador         int  NOT NULL    PRIMARY KEY,
	codigo               bigint      ,
	origen               varchar(100)      ,
	destino              varchar(100)      ,
	fecha                date      ,
	hora                 time      ,
	fecha_hora_simulador timestamp   DEFAULT CURRENT_TIMESTAMP   ,
	pisoa                int      ,
	pisob                int      ,
	precio               decimal(12,2)      ,
	CONSTRAINT fk_simulado_contacto FOREIGN KEY ( codigo ) REFERENCES tesisvalderrama.contacto( codigo ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

CREATE INDEX fk_simulado_contacto ON tesisvalderrama.simulador ( codigo );

CREATE TABLE tesisvalderrama.usuario ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	persona              bigint  NOT NULL    ,
	contrasena           char(32)  NOT NULL    ,
	estado               char(1)  NOT NULL DEFAULT 'A'   ,
	foto                 varchar(50)  NOT NULL DEFAULT 'default.jpg'   ,
	sesiones             bigint  NOT NULL DEFAULT 0   ,
	CONSTRAINT usuario_ibfk_1 FOREIGN KEY ( persona ) REFERENCES tesisvalderrama.persona( codigo ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX personal ON tesisvalderrama.usuario ( persona );

CREATE TABLE tesisvalderrama.vehiculo ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	nombre               varchar(255)      ,
	imagen               varchar(255)      ,
	placa                varchar(50)      ,
	ancho                decimal(10,2)   DEFAULT 0.00   ,
	alto                 decimal(10,2)   DEFAULT 0.00   ,
	largo                decimal(10,2)   DEFAULT 0.00   ,
	estado               char(1)   DEFAULT 'A'   
 );

CREATE TABLE tesisvalderrama.galeria ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	titulo               varchar(255)      ,
	descripcion          text      ,
	imagen               varchar(255)      ,
	tipo                 bigint      ,
	estado               char(1)   DEFAULT 'A'   ,
	CONSTRAINT galeria_ibfk_1 FOREIGN KEY ( tipo ) REFERENCES tesisvalderrama.galeria_tipo( codigo ) ON DELETE CASCADE ON UPDATE RESTRICT
 );

CREATE INDEX galeria_ibfk_1 ON tesisvalderrama.galeria ( tipo );

CREATE TABLE tesisvalderrama.objeto ( 
	codigo               bigint  NOT NULL    PRIMARY KEY,
	nombre               varchar(255)      ,
	imagen               varchar(255)      ,
	ancho                decimal(10,2)   DEFAULT 0.00   ,
	alto                 decimal(10,2)   DEFAULT 0.00   ,
	largo                decimal(10,2)   DEFAULT 0.00   ,
	tipo                 bigint      ,
	estado               char(1)   DEFAULT 'A'   ,
	CONSTRAINT objeto_ibfk_1 FOREIGN KEY ( tipo ) REFERENCES tesisvalderrama.objeto_tipo( codigo ) ON DELETE RESTRICT ON UPDATE RESTRICT
 );

CREATE INDEX tipo ON tesisvalderrama.objeto ( tipo );

CREATE TABLE tesisvalderrama.simulador_detalle ( 
	id_simulador         int  NOT NULL    ,
	item                 int  NOT NULL    ,
	codigo               bigint  NOT NULL    ,
	cantidad             int  NOT NULL    ,
	CONSTRAINT pk_simulador_detalle_id_simulador PRIMARY KEY ( id_simulador, item, codigo ),
	CONSTRAINT fk_simulador_detalle_simulador FOREIGN KEY ( id_simulador ) REFERENCES tesisvalderrama.simulador( id_simulador ) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT fk_simulador_detalle_objeto FOREIGN KEY ( codigo ) REFERENCES tesisvalderrama.objeto( codigo ) ON DELETE NO ACTION ON UPDATE NO ACTION
 ) engine=InnoDB;

INSERT INTO tesisvalderrama.cargo( codigo, descripcion, estado ) VALUES ( 1, 'Administrador', 'A' ); 
INSERT INTO tesisvalderrama.cargo( codigo, descripcion, estado ) VALUES ( 2, 'Cliente', 'A' ); 
INSERT INTO tesisvalderrama.cargo( codigo, descripcion, estado ) VALUES ( 3, 'Personal', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 2, 'juan diego', 'Vasquez valderrama', null, 'jdiegovval@gmail.com', 'mudanza de chiclayo a piura', 'hola de prueba', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 3, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'zsdcgfvhgjnhkjml', 'rsdcvfgbhnjmkl', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 4, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'viaje', 'hola papi', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 5, 'ROSALIA', 'SANTISTEBAN', '953384901', 'rosalia_15_2003@hotmil.com', 'MUDNZA', 'completa', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 6, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'mudanza peru', 'mudanza peru', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 7, 'Génesis', 'Contreras morales', '996831775', 'genesisbsb8@gmail.com', 'Felicidades', 'Eres el mejor ING. En sistemas para estos ojitos TeQuieroenormemente???? vamos por más ????', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 8, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 9, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 10, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 11, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 12, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 13, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 14, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 15, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 16, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 17, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 18, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 19, 'a', 'b', 'c', 'd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 20, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 21, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 22, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 23, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 24, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 25, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 26, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 27, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 28, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 29, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 30, 'a', 'q', 'q', 'w', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 31, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 32, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 33, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 34, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 35, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 36, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 37, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 38, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 39, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 40, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 41, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 42, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 43, 'dasJK', 'KJHJK', 'HJK', 'HKJH', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 44, 'j', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 45, 'j', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 46, 'j', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 47, 'j', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 48, 'j', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 49, 'aj', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 50, 'aj', 'j', 'j', 'j', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 51, '12', '123', '123', '123', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 52, '12', '23', '123', '23', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 53, 'a', 'a', 'a', 'a', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 54, 'a', 'a', 'a', 'a', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 55, 'a', 'a', 'a', 'a', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 56, 'a', 'a', 'a', 'a', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 57, 'a', 'a', 'a', 'a', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 58, 'qw', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 59, 'qw', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 60, 'kjl', 'jkjk', 'jk', 'jkjk', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 61, 'kk', 'kk', 'kk', 'kk', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 62, 'kk', 'kk', 'kk', 'kk', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 63, 'jk', 'jk', 'jkjk', 'jk', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 64, 'jk', 'jk', 'jkjk', 'jk', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 65, 'qwe', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 66, 'qwe', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 67, 'qwe', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 68, 'qwe', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 69, 'ASD', 'ASD', 'ASD', 'ASD', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 70, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 71, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 72, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 73, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 74, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 75, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 76, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 77, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 78, 'asd', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 79, 'hjk', 'hjkjk', 'jkh', 'jkh', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 80, 'hjk', 'hjkjk', 'jkh', 'jkh', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 81, '123', '123', '123', '231', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 82, '123', '123', '123', '231', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 83, 'kl', 'jkl', 'jklj', 'kljl', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 84, 'ad', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 85, '2', '1', '1', '2', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 86, '2', '2', '2', '2', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 87, 'as', 'asd', 'asd', 'asd', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 88, 'aqwe', 'qwe', 'qwe', 'qwe', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 89, 'sdf', 'sdf', 'klj', 'ljkl', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 90, 'sdf', 'sdf', 'klj', 'ljkl', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 91, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 92, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 93, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 94, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 95, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 96, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 97, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 98, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 99, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 100, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 101, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 102, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje, estado ) VALUES ( 103, 'asdf', 'asdf', 'asdf', 'asdf', 'simulador', '', 'A' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 2, 'Mud Chiclayo', 'A' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 3, 'Mud Piura', 'I' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 4, 'Mud Trujillo', 'I' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 5, 'Mud Norte Peruano', 'A' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 6, 'Transporte de bienes ', 'A' ); 
INSERT INTO tesisvalderrama.galeria_tipo( codigo, nombre, estado ) VALUES ( 7, 'Embalaje', 'A' ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 1, 'fa-circle-o', 'Privilegios', '', 'A', 1, 3, '', null ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 2, 'fa-circle-o', 'Accesos', '', 'A', 2, 1, 'accesos', 1 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 3, 'fa-circle-o', 'Mantenimiento', '', 'A', 1, 1, '', null ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 4, 'fa-circle-o', 'Personal', '', 'A', 2, 1, 'personal', 3 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 5, 'fa-circle-o', 'Tipo de Objeto', '', 'A', 2, 2, 'objeto_tipo', 3 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 6, 'fa-circle-o', 'Objeto', '', 'A', 2, 3, 'objeto', 3 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 7, 'fa-circle-o', 'Vehículo', '', 'A', 2, 4, 'vehiculo', 3 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 8, 'fa-circle-o', 'Contáctanos', '', 'A', 1, 4, 'contacto', null ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 9, 'fa-circle-o', 'Galería', '', 'A', 1, 2, '', null ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 10, 'fa-circle-o', 'Categoría', '', 'A', 2, 1, 'galeria_categoria', 9 ); 
INSERT INTO tesisvalderrama.menu( codigo_menu, icono, nombre, descripcion, estado, nivel, orden, archivo, id_padre ) VALUES ( 11, 'fa-circle-o', 'Imagen', '', 'A', 2, 2, 'galeria_imagen', 9 ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 1, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 1, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 1, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 2, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 2, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 2, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 3, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 3, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 3, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 4, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 4, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 4, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 5, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 5, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 5, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 6, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 6, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 6, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 7, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 7, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 7, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 8, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 8, 2, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 8, 3, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 9, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 9, 2, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 9, 3, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 10, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 10, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 10, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 11, 1, '1', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 11, 2, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.menu_accesos( menu, cargo, acceso, agregar, modificar, eliminar, imprimir ) VALUES ( 11, 3, '0', '1', '1', '1', '1' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 1, 'Título de prueba', 'asdas', 'I' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 2, 'Artefactos eléctricos ', 'Artefactos eléctricos ', 'I' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 3, 'Refrigeradoras', 'Electrodomésticos de cocina', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 4, 'Lavadoras', 'Electrodomésticos de patio', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 5, 'Cocinas', 'Electrodomésticos de cocina', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 6, 'Colchones', 'Muebles de Cuarto', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 7, 'Mesas', 'Muebles de comedor', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 8, 'Sillas', 'Muebles de Comedor', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 9, 'TV', 'Electrodomésticos ', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 10, 'Juego de Sofá 3-2-1', 'Muebles de sentarse', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 11, 'Centro de entretenimiento', 'Mueble', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 12, 'Ropero', 'Mueble de madera o melamina', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 13, 'Cómoda', 'mueble ', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 14, 'Tarima', 'Mueble de habitación', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 15, 'Velador', 'Mueble de habitación', 'A' ); 
INSERT INTO tesisvalderrama.objeto_tipo( codigo, nombre, descripcion, estado ) VALUES ( 16, 'Sofá cama', 'mueble', 'A' ); 
INSERT INTO tesisvalderrama.persona( codigo, dni, nombres, apellido_paterno, apellido_materno, sexo, telefono, email, cargo, estado ) VALUES ( 1, '70269322', 'Admin', 'Admin', 'Admin', 0, '924-497-770', 'admin@gmail.com', 1, 'A' ); 
INSERT INTO tesisvalderrama.persona( codigo, dni, nombres, apellido_paterno, apellido_materno, sexo, telefono, email, cargo, estado ) VALUES ( 2, '22222222', 'jose diego', 'vasquez', 'fernandez', 0, '997527651', 'josediego@gmail.com', 3, 'A' ); 
INSERT INTO tesisvalderrama.persona( codigo, dni, nombres, apellido_paterno, apellido_materno, sexo, telefono, email, cargo, estado ) VALUES ( 3, '11111111', 'lia', 'santisteban', 'valderrama', 0, '953384901', 'lia@gmail.com', 2, 'A' ); 
INSERT INTO tesisvalderrama.persona( codigo, dni, nombres, apellido_paterno, apellido_materno, sexo, telefono, email, cargo, estado ) VALUES ( 4, '12341234', 'mvilchez', 'mvilchez', 'mvilchez', 1, '12341234', 'mvilchez@gmail.com', 1, 'A' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 1, 2, 'Mike watches football. Rudi watches football. ', 'Mike like swimming. Rudi like sports. ', '2016-03-19', null, '2008-03-17 12:45:57', null, null, 7700942285.14, 'r', 'k', 'Tony has free time. Anne has free time. Tony bought new car. ', 'Anne bought new car. Anne bought new car. John bought new car. Tony has free time. ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 2, 17, 'g', 'h', '2007-02-20', '10:46 AM', '2020-07-16 15:50:10', 1, 2, 200.00, 'b', 'c', 'e', 'f' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 3, 18, 'g', 'h', '2016-07-20', '10:54 AM', '2020-07-16 15:54:12', 1, 2, 200.00, 'b', 'e', 'e', 'f' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 4, 19, 'g', 'h', '2016-07-20', '10:54 AM', '2020-07-16 16:33:01', 1, 2, 200.00, 'b', 'e', 'e', 'f' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 5, 20, 'q', 'q', '2016-07-20', '11:33 AM', '2020-07-16 16:33:33', 1, 2, 200.00, 'd', 'h', 'q', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 6, 21, 'q', 'q', '2016-07-20', '11:33 AM', '2020-07-16 16:34:35', 1, 2, 200.00, 'd', 'h', 'q', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 7, 22, 'q', 'q', '2016-07-20', '11:33 AM', '2020-07-16 16:35:14', 1, 2, 200.00, 'd', 'h', 'q', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 8, 23, 'q', 'q', '2016-07-20', '11:33 AM', '2020-07-16 16:35:25', 1, 2, 200.00, 'd', 'h', 'q', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 9, 24, 'q', 'q', '2016-07-20', '11:33 AM', '2020-07-16 16:35:40', 1, 2, 200.00, 'd', 'h', 'q', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 10, 25, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:36:53', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 11, 26, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:41:48', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 12, 27, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:42:27', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 13, 28, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:43:09', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 14, 29, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:57:21', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 15, 30, 'a', 's', '2016-07-20', '11:36 AM', '2020-07-16 16:58:27', 1, 2, 200.00, 'b', 'e', 'wq', 'w' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 16, 31, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-16 16:58:57', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 17, 32, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-16 23:47:31', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 18, 33, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:21:33', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 19, 34, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:27:55', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 20, 35, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:28:52', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 21, 36, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:31:17', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 22, 37, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:42:54', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 23, 38, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:44:34', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 24, 39, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:48:09', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 25, 40, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:53:31', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 26, 41, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:53:53', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 27, 42, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:54:29', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 28, 43, 'HKJ', 'HKJH', '2016-07-20', '11:58 AM', '2020-07-17 00:56:04', 12, 2, 200.00, 'b', 'e', 'KJH', 'JKHKJ' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 29, 44, '1', '12', '2016-07-20', '8:06 PM', '2020-07-17 01:06:58', 21, 1, 200.00, 'b', 'e', 'j', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 30, 45, '1', '12', '2016-07-20', '8:06 PM', '2020-07-17 01:10:04', 21, 1, 200.00, 'b', 'e', 'j', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 31, 46, '1', '12', '2016-07-20', '8:06 PM', '2020-07-17 02:19:22', 21, 1, 200.00, 'b', 'e', 'j', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 32, 47, '1', '12', '2016-07-20', '8:06 PM', '2020-07-17 02:27:20', 21, 1, 200.00, 'b', 'e', 'j', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 33, 48, '1', '12', '2016-07-20', '8:06 PM', '2020-07-17 02:43:46', 21, 1, 200.00, 'b', 'e', 'j', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 34, 49, 'j', 'j', '2016-07-20', '9:44 PM', '2020-07-17 02:44:50', 1, 2, 200.00, 'b', 'e', 'jj', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 35, 50, 'j', 'j', '2016-07-20', '9:44 PM', '2020-07-17 02:45:52', 1, 2, 200.00, 'b', 'e', 'jj', 'j' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 36, 51, '1', '3', '2018-07-20', '8:22 AM', '2020-07-18 13:27:26', 1, 2, 200.00, 'b', 'h', '123', '123' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 37, 52, '123', '213', '2018-07-20', '8:47 AM', '2020-07-18 13:48:20', 123, 123, 200.00, 'b', 'e', '123', '123' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 38, 53, 'a', 'a', '2018-07-20', '8:53 AM', '2020-07-18 13:54:01', 2, 1, 200.00, 'o', 'e', 'qa', 'a' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 39, 54, 'a', 'a', '2018-07-20', '8:53 AM', '2020-07-18 13:54:29', 2, 1, 200.00, 'o', 'e', 'qa', 'a' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 40, 55, 'a', 'a', '2018-07-20', '8:53 AM', '2020-07-18 13:55:47', 2, 1, 200.00, 'o', 'e', 'qa', 'a' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 41, 56, 'a', 'a', '2018-07-20', '8:53 AM', '2020-07-18 13:59:17', 2, 1, 200.00, 'o', 'e', 'qa', 'a' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 42, 57, 'a', 'a', '2018-07-20', '8:59 AM', '2020-07-18 14:00:04', 1, 2, 200.00, 'b', 'e', 'a', 'a' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 43, 58, 'qwe', 'wqe', '2018-07-20', '9:10 AM', '2020-07-18 14:10:53', 1, 2, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 44, 59, 'qwe', 'wqe', '2018-07-20', '9:10 AM', '2020-07-18 14:10:55', 1, 2, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 45, 60, 'ed', 'e', '2018-07-20', '9:15 AM', '2020-07-18 14:15:51', 1, 2, 200.00, 'o', 'e', 'jk', 'jkjk' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 46, 61, '1', '2', '2018-07-20', '9:35 AM', '2020-07-18 14:36:24', 1, 2, 200.00, 'b', 'e', 'k', 'kk' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 47, 62, '1', '2', '2018-07-20', '9:35 AM', '2020-07-18 14:36:27', 1, 2, 200.00, 'b', 'e', 'k', 'kk' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 48, 63, 'we', 'qwe', '2018-07-20', '9:39 AM', '2020-07-18 14:39:44', 1, 2, 200.00, 'b', 'h', 'jkjk', 'jk' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 49, 64, 'we', 'qwe', '2018-07-20', '9:39 AM', '2020-07-18 14:46:41', 1, 2, 200.00, 'b', 'h', 'jkjk', 'jk' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 50, 65, 'qwe', 'qwe', '2018-07-20', '11:55 AM', '2020-07-18 16:56:17', 12, 23, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 51, 66, 'qwe', 'qwe', '2018-07-20', '11:55 AM', '2020-07-18 17:02:28', 12, 23, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 52, 67, 'qwe', 'qwe', '2018-07-20', '11:55 AM', '2020-07-18 17:03:23', 12, 23, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 53, 68, 'qwe', 'qwe', '2018-07-20', '11:55 AM', '2020-07-18 17:03:31', 12, 23, 200.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 54, 69, 'ASD', 'ASD', '2018-07-20', '12:09 PM', '2020-07-18 17:10:44', 11, 2, 200.00, 'b', 'e', 'ASD', 'ASD' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 55, 70, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 17:14:13', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 56, 71, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 17:34:59', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 57, 72, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 17:35:06', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 58, 73, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 18:30:25', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 59, 74, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 18:59:13', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 60, 75, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 19:01:39', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 61, 76, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 19:04:29', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 62, 77, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 19:14:28', 1, 2, 200.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 63, 78, 'asd', 'asd', '2018-07-20', '12:14 PM', '2020-07-18 19:17:58', 1, 2, 200.00, 'b', 'c', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 64, 79, 'as', 'asdf', '2018-07-20', '2:38 PM', '2020-07-18 19:39:02', 1, 2, 200.00, 'b', 'e', 'jkh', 'kj' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 65, 80, 'as', 'asdf', '2018-07-20', '2:38 PM', '2020-07-18 19:49:06', 6, 2, 200.00, 'd', 'e', 'jkh', 'kj' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 66, 81, '123', '123', '2018-07-20', '3:30 PM', '2020-07-18 20:30:57', 1, 2, 110.00, 'b', 'h', '123', '231' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 67, 82, '123', '123', '2018-07-20', '3:30 PM', '2020-07-18 21:21:52', 1, 2, 110.00, 'b', 'h', '123', '231' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 68, 83, 'lkjlk', 'jlkj', '2018-07-20', '6:16 PM', '2020-07-18 23:16:26', 1, 4, 150.00, 'b', 'e', 'kjlk', 'jlkj' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 69, 84, 'asd', 'asd', '2018-07-20', '6:17 PM', '2020-07-18 23:17:33', 1, 2, 110.00, 'b', 'h', 'asd', 'asd' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 70, 85, '1', '2', '2018-07-20', '6:20 PM', '2020-07-18 23:20:58', 1, 2, 110.00, 'b', 'h', '1', '2' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 71, 86, '1', '2', '2018-07-20', '6:21 PM', '2020-07-18 23:21:26', 1, 2, 110.00, 'b', 'e', '2', '2' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 72, 87, 'asd', 'asd', '2018-07-20', '6:25 PM', '2020-07-18 23:25:27', 1, 3, 130.00, 'b', 'e', 'asd', 'dsa' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 73, 88, 'qwe', 'eqw', '2018-07-20', '6:32 PM', '2020-07-18 23:32:15', 1, 4, 150.00, 'b', 'e', 'qwe', 'qwe' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 74, 89, 'lkj', 'lkj', '2019-07-20', '9:22 AM', '2020-07-19 14:23:18', 2, 5, 200.00, 'o', 'e', 'jlk', 'jlkj' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 75, 90, 'lkj', 'lkj', '2019-07-20', '9:22 AM', '2020-07-19 14:25:12', 2, 5, 200.00, 'o', 'e', 'jlk', 'jlkj' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 76, 91, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:16:56', 2, 1, 110.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 77, 92, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:26:13', 2, 1, 110.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 78, 93, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:34:52', 2, 1, 110.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 79, 94, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:35:05', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 80, 95, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:36:52', 2, 1, 110.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 81, 96, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:42:42', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 82, 97, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:48:23', 2, 1, 110.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 83, 98, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:49:06', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 84, 99, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:50:38', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 85, 100, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 15:56:55', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 86, 101, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 16:08:20', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 87, 102, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 16:09:37', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.simulador( id_simulador, codigo, origen, destino, fecha, hora, fecha_hora_simulador, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia ) VALUES ( 88, 103, 'asdf', 'asdf', '2019-07-20', '10:16 AM', '2020-07-19 16:10:28', 2, 1, 140.00, 'b', 'h', 'asdf', 'adfs' ); 
INSERT INTO tesisvalderrama.usuario( codigo, persona, contrasena, estado, foto, sesiones ) VALUES ( 1, 1, '202cb962ac59075b964b07152d234b70', 'A', 'default.jpg', 216 ); 
INSERT INTO tesisvalderrama.usuario( codigo, persona, contrasena, estado, foto, sesiones ) VALUES ( 2, 2, 'bae5e3208a3c700e3db642b6631e95b9', 'A', 'default.jpg', 3 ); 
INSERT INTO tesisvalderrama.usuario( codigo, persona, contrasena, estado, foto, sesiones ) VALUES ( 3, 3, '1bbd886460827015e5d605ed44252251', 'A', 'default.jpg', 4 ); 
INSERT INTO tesisvalderrama.usuario( codigo, persona, contrasena, estado, foto, sesiones ) VALUES ( 4, 4, 'ed2b1f468c5f915f3f1cf75d7068baae', 'A', 'default.jpg', 1 ); 
INSERT INTO tesisvalderrama.vehiculo( codigo, nombre, imagen, placa, ancho, alto, largo, estado ) VALUES ( 1, 'Camión 1', '1.png', 'M5K-819', 2.00, 2.50, 2.50, 'A' ); 
INSERT INTO tesisvalderrama.vehiculo( codigo, nombre, imagen, placa, ancho, alto, largo, estado ) VALUES ( 2, 'Camión 2', '2.png', 'M1L-333', 2.50, 3.00, 5.00, 'A' ); 
INSERT INTO tesisvalderrama.vehiculo( codigo, nombre, imagen, placa, ancho, alto, largo, estado ) VALUES ( 3, 'Camión 3', '3.png', 'M1L-333', 3.00, 3.50, 8.50, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 3, 'mud', 'xrctvygbuh', '3.jpg', 3, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 4, 'pruebaa', 'pruebaaa', '4.jpg', 4, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 6, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque  ', '6.png', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 7, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '7.png', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 8, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '8.png', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 9, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '9.png', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 10, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '10.png', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 12, 'Trans. Bienes y Mercancías', 'Transporte de Bienes y Mercancías para distintas empresas', '12.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 13, 'Corso alegórico', 'Transporte de Bienes y Mercancías para distintas empresas', '13.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 14, 'Carga Ipesa Hydro', 'Servicio  de carga para la empresa Ipesa Hydro', '14.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 15, 'Trans. Bienes y Mercancías', 'Traslado de trompo para empresa constructora ', '15.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 16, 'Trans. Bienes y Mercancías-Chota', 'Traslado de accesorios para la provincia de Chota', '16.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 17, 'Proyecto Olmos', 'Traslado de tanques para el proyecto olmos', '17.jpg', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 18, 'Embalaje', 'embalaje', '18.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 19, 'Embalaje', 'Servicio de embalaje ', '19.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 20, 'Embalaje', 'Servicio de embalaje ', '20.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 21, 'Embalaje', 'Servicio de embalaje ', '21.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 22, 'Embalaje', 'Servicio de embalaje ', '22.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 23, 'Embalaje', 'Servicio de embalaje', '23.png', 7, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 24, 'Traslado de un tractor', 'Traslado de un tractor para el proyecto Agro Olmos', '24.png', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 25, 'Traslado de una vaca', 'Traslado de una vaca hasta el departamento de Piura ', '25.jpg', 6, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 26, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque ', '26.jpg', 2, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 27, 'Mud Piura', 'Mudanza para el departamento de Piura', '27.png', 5, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 28, 'Mud Tumbes', 'Mudanza en el departamento de Tumbes', '28.png', 5, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 29, 'Mud Jaen', 'Mudanza en la Provincia de Jaen', '29.png', 5, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 30, 'Mud Piura', 'Una pequeña mudanza en el departamento de Piura', '30.jpg', 5, 'A' ); 
INSERT INTO tesisvalderrama.galeria( codigo, titulo, descripcion, imagen, tipo, estado ) VALUES ( 31, 'Mud Tumbes', 'Servicio de Mudanza para el departamento de Tumbes - Puyango', '31.png', 5, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 1, 'refrigerador 1 puerta', '1.jpg', 2.00, 0.50, 0.50, 1, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 2, 'frigobar', '2.jpg', 0.47, 0.85, 0.45, 2, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 3, 'Frigobar', '3.jpg', 0.47, 0.85, 0.45, 3, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 4, 'Refrigeradora', '4.jpg', 0.70, 1.68, 0.73, 3, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 5, 'Refrigeradora 2 puertas', '5.jpg', 0.92, 1.79, 0.74, 3, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 6, 'Lavadora 12 KG', '6.jpg', 0.60, 0.97, 0.63, 4, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 7, 'Lavadora 20 KG', '7.jpg', 0.68, 1.08, 0.68, 4, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 8, 'Portatil', '8.jpg', 0.53, 0.12, 0.50, 5, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 9, '4 hornillas', '9.jpg', 0.51, 0.95, 0.63, 5, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 10, '6 hornillas', '10.jpg', 0.80, 0.94, 0.58, 5, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 11, 'Colchón 1.5 plazas', '11.jpg', 1.05, 0.20, 1.90, 6, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 12, 'Colchón 2 plazas', '12.jpg', 1.35, 0.30, 1.90, 6, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 13, 'Colchón Queen', '13.png', 1.53, 0.35, 2.03, 6, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 14, 'Colchón King', '14.png', 1.98, 0.36, 2.03, 6, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 15, 'Mesa 4 sillas', '15.jpg', 1.20, 0.78, 0.80, 7, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 16, 'Mesa 6 sillas', '16.png', 1.60, 0.78, 0.80, 7, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 17, 'Mesa 8 sillas', '17.png', 1.80, 0.78, 1.00, 7, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 18, 'silla', '18.jpg', 0.42, 1.03, 0.54, 8, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 19, 'TV caja', '19.jpg', 0.65, 0.50, 0.65, 9, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 20, 'TV 32"', '20.jpg', 0.71, 0.40, 0.20, 9, 'A' ); 
INSERT INTO tesisvalderrama.objeto( codigo, nombre, imagen, ancho, alto, largo, tipo, estado ) VALUES ( 21, 'TV 52"', '21.jpg', 1.15, 0.65, 0.28, 9, 'A' ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 2, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 2, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 3, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 3, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 4, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 4, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 5, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 6, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 7, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 8, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 9, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 10, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 11, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 12, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 13, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 14, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 15, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 16, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 16, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 17, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 17, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 18, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 18, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 19, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 19, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 20, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 20, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 21, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 21, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 22, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 22, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 23, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 23, 2, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 24, 1, 19, 22 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 24, 2, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 25, 1, 19, 22 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 25, 2, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 26, 1, 19, 22 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 26, 2, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 27, 1, 19, 22 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 27, 2, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 28, 1, 19, 22 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 28, 2, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 29, 1, 19, 32 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 30, 1, 19, 32 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 1, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 2, 7, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 3, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 4, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 5, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 6, 3, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 31, 7, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 1, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 2, 7, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 3, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 4, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 5, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 6, 3, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 7, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 8, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 32, 9, 21, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 1, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 2, 7, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 3, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 4, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 5, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 6, 3, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 7, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 8, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 33, 9, 21, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 1, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 2, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 3, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 4, 6, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 5, 7, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 6, 15, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 7, 16, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 8, 17, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 9, 3, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 10, 4, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 11, 5, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 12, 18, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 13, 1, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 14, 19, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 34, 15, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 1, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 2, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 3, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 4, 6, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 5, 7, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 6, 15, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 7, 16, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 8, 17, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 9, 3, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 10, 4, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 11, 5, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 12, 18, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 13, 1, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 14, 19, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 35, 15, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 1, 19, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 2, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 3, 3, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 4, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 5, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 6, 18, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 7, 15, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 8, 16, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 9, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 10, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 36, 11, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 1, 2, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 2, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 3, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 4, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 5, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 6, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 7, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 8, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 9, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 10, 7, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 11, 15, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 12, 16, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 13, 17, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 14, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 15, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 16, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 17, 18, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 18, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 19, 19, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 20, 20, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 37, 21, 21, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 1, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 2, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 3, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 4, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 5, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 6, 2, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 7, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 8, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 38, 9, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 1, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 2, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 3, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 4, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 5, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 6, 2, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 7, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 8, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 39, 9, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 1, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 2, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 3, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 4, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 5, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 6, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 7, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 8, 2, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 9, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 10, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 40, 11, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 1, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 2, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 3, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 4, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 5, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 6, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 7, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 8, 2, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 9, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 10, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 41, 11, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 42, 1, 4, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 42, 2, 2, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 42, 3, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 42, 4, 18, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 42, 5, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 1, 2, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 2, 15, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 3, 9, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 4, 10, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 5, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 6, 20, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 7, 19, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 43, 8, 8, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 1, 2, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 2, 15, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 3, 9, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 4, 10, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 5, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 6, 20, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 7, 19, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 44, 8, 8, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 1, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 2, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 3, 2, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 4, 9, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 5, 10, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 6, 6, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 7, 7, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 8, 15, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 9, 16, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 10, 17, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 11, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 12, 8, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 13, 11, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 14, 12, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 15, 13, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 16, 14, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 45, 17, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 1, 4, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 2, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 3, 21, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 4, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 5, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 6, 19, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 7, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 8, 11, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 9, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 10, 13, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 48, 11, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 1, 4, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 2, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 3, 21, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 4, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 5, 15, 4 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 6, 16, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 7, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 8, 19, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 9, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 10, 11, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 11, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 12, 13, 1 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 49, 13, 14, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 1, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 2, 5, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 3, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 4, 1, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 5, 11, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 6, 12, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 7, 13, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 8, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 50, 9, 20, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 51, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 51, 2, 3, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 51, 3, 15, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 51, 4, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 51, 5, 20, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 52, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 52, 2, 3, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 52, 3, 15, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 52, 4, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 52, 5, 20, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 53, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 53, 2, 3, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 53, 3, 15, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 53, 4, 19, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 53, 5, 20, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 54, 1, 3, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 54, 2, 1, 11 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 54, 3, 19, 11 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 54, 4, 20, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 55, 1, 4, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 55, 2, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 55, 3, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 55, 4, 11, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 56, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 56, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 57, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 57, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 58, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 58, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 59, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 59, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 60, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 60, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 60, 3, 13, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 60, 4, 14, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 61, 1, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 61, 2, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 61, 3, 13, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 61, 4, 12, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 61, 5, 14, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 1, 4, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 2, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 3, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 4, 13, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 5, 12, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 62, 6, 14, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 1, 4, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 2, 3, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 3, 19, 15 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 4, 13, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 5, 12, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 63, 6, 14, 12 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 64, 1, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 64, 2, 15, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 64, 3, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 64, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 64, 5, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 65, 1, 4, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 65, 2, 15, 6 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 65, 3, 3, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 65, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 65, 5, 20, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 66, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 67, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 68, 1, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 68, 2, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 69, 1, 3, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 70, 1, 1, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 71, 1, 19, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 72, 1, 19, 5 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 73, 1, 1, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 74, 1, 2, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 75, 1, 2, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 75, 2, 15, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 75, 3, 16, 3 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 76, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 77, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 78, 1, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 78, 2, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 79, 1, 21, 16 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 79, 2, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 79, 3, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 80, 1, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 80, 2, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 80, 3, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 81, 1, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 81, 2, 3, 21 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 81, 3, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 81, 4, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 82, 1, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 82, 2, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 82, 3, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 83, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 83, 2, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 83, 3, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 83, 4, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 84, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 84, 2, 5, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 84, 3, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 84, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 84, 5, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 85, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 85, 2, 5, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 85, 3, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 85, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 85, 5, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 86, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 86, 2, 5, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 86, 3, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 86, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 86, 5, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 87, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 87, 2, 5, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 87, 3, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 87, 4, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 87, 5, 20, 8 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 1, 4, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 2, 5, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 3, 21, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 4, 18, 10 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 5, 19, 2 ); 
INSERT INTO tesisvalderrama.simulador_detalle( id_simulador, item, codigo, cantidad ) VALUES ( 88, 6, 20, 8 ); 

/*
 Navicat Premium Data Transfer

 Source Server         : VortiPro
 Source Server Type    : MariaDB
 Source Server Version : 100038
 Source Host           : localhost:3306
 Source Schema         : ciclon

 Target Server Type    : MariaDB
 Target Server Version : 100038
 File Encoding         : 65001

 Date: 11/07/2020 12:20:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo`  (
  `codigo` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cargo
-- ----------------------------
INSERT INTO `cargo` VALUES (1, 'Administrador', 'A');
INSERT INTO `cargo` VALUES (2, 'Cliente', 'A');
INSERT INTO `cargo` VALUES (3, 'Personal', 'A');

-- ----------------------------
-- Table structure for contacto
-- ----------------------------
DROP TABLE IF EXISTS `contacto`;
CREATE TABLE `contacto`  (
  `codigo` bigint(20) NOT NULL,
  `nombres` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `apellidos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `telefono` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `asunto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mensaje` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacto
-- ----------------------------
INSERT INTO `contacto` VALUES (2, 'juan diego', 'Vasquez valderrama', NULL, 'jdiegovval@gmail.com', 'mudanza de chiclayo a piura', 'hola de prueba', 'A');
INSERT INTO `contacto` VALUES (3, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'zsdcgfvhgjnhkjml', 'rsdcvfgbhnjmkl', 'A');
INSERT INTO `contacto` VALUES (4, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'viaje', 'hola papi', 'A');
INSERT INTO `contacto` VALUES (5, 'ROSALIA', 'SANTISTEBAN', '953384901', 'rosalia_15_2003@hotmil.com', 'MUDNZA', 'completa', 'A');
INSERT INTO `contacto` VALUES (6, 'juan diego', 'vasquez valderrama', '997527651', 'jdiegovval@gmail.com', 'mudanza peru', 'mudanza peru', 'A');
INSERT INTO `contacto` VALUES (7, 'Génesis', 'Contreras morales', '996831775', 'genesisbsb8@gmail.com', 'Felicidades', 'Eres el mejor ING. En sistemas para estos ojitos TeQuieroenormemente???? vamos por más ????', 'A');

-- ----------------------------
-- Table structure for galeria
-- ----------------------------
DROP TABLE IF EXISTS `galeria`;
CREATE TABLE `galeria`  (
  `codigo` bigint(20) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tipo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `galeria_ibfk_1`(`tipo`) USING BTREE,
  CONSTRAINT `galeria_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `galeria_tipo` (`codigo`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of galeria
-- ----------------------------
INSERT INTO `galeria` VALUES (3, 'mud', 'xrctvygbuh', '3.jpg', 3, 'A');
INSERT INTO `galeria` VALUES (4, 'pruebaa', 'pruebaaa', '4.jpg', 4, 'A');
INSERT INTO `galeria` VALUES (6, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque  ', '6.png', 2, 'A');
INSERT INTO `galeria` VALUES (7, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '7.png', 2, 'A');
INSERT INTO `galeria` VALUES (8, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '8.png', 2, 'A');
INSERT INTO `galeria` VALUES (9, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '9.png', 2, 'A');
INSERT INTO `galeria` VALUES (10, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque', '10.png', 2, 'A');
INSERT INTO `galeria` VALUES (12, 'Trans. Bienes y Mercancías', 'Transporte de Bienes y Mercancías para distintas empresas', '12.png', 6, 'A');
INSERT INTO `galeria` VALUES (13, 'Corso alegórico', 'Transporte de Bienes y Mercancías para distintas empresas', '13.png', 6, 'A');
INSERT INTO `galeria` VALUES (14, 'Carga Ipesa Hydro', 'Servicio  de carga para la empresa Ipesa Hydro', '14.png', 6, 'A');
INSERT INTO `galeria` VALUES (15, 'Trans. Bienes y Mercancías', 'Traslado de trompo para empresa constructora ', '15.png', 6, 'A');
INSERT INTO `galeria` VALUES (16, 'Trans. Bienes y Mercancías-Chota', 'Traslado de accesorios para la provincia de Chota', '16.png', 6, 'A');
INSERT INTO `galeria` VALUES (17, 'Proyecto Olmos', 'Traslado de tanques para el proyecto olmos', '17.jpg', 6, 'A');
INSERT INTO `galeria` VALUES (18, 'Embalaje', 'embalaje', '18.png', 7, 'A');
INSERT INTO `galeria` VALUES (19, 'Embalaje', 'Servicio de embalaje ', '19.png', 7, 'A');
INSERT INTO `galeria` VALUES (20, 'Embalaje', 'Servicio de embalaje ', '20.png', 7, 'A');
INSERT INTO `galeria` VALUES (21, 'Embalaje', 'Servicio de embalaje ', '21.png', 7, 'A');
INSERT INTO `galeria` VALUES (22, 'Embalaje', 'Servicio de embalaje ', '22.png', 7, 'A');
INSERT INTO `galeria` VALUES (23, 'Embalaje', 'Servicio de embalaje', '23.png', 7, 'A');
INSERT INTO `galeria` VALUES (24, 'Traslado de un tractor', 'Traslado de un tractor para el proyecto Agro Olmos', '24.png', 6, 'A');
INSERT INTO `galeria` VALUES (25, 'Traslado de una vaca', 'Traslado de una vaca hasta el departamento de Piura ', '25.jpg', 6, 'A');
INSERT INTO `galeria` VALUES (26, 'Mud CIX', 'Mudanzas en todo el departamento de Lambayeque ', '26.jpg', 2, 'A');
INSERT INTO `galeria` VALUES (27, 'Mud Piura', 'Mudanza para el departamento de Piura', '27.png', 5, 'A');
INSERT INTO `galeria` VALUES (28, 'Mud Tumbes', 'Mudanza en el departamento de Tumbes', '28.png', 5, 'A');
INSERT INTO `galeria` VALUES (29, 'Mud Jaen', 'Mudanza en la Provincia de Jaen', '29.png', 5, 'A');
INSERT INTO `galeria` VALUES (30, 'Mud Piura', 'Una pequeña mudanza en el departamento de Piura', '30.jpg', 5, 'A');
INSERT INTO `galeria` VALUES (31, 'Mud Tumbes', 'Servicio de Mudanza para el departamento de Tumbes - Puyango', '31.png', 5, 'A');

-- ----------------------------
-- Table structure for galeria_tipo
-- ----------------------------
DROP TABLE IF EXISTS `galeria_tipo`;
CREATE TABLE `galeria_tipo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of galeria_tipo
-- ----------------------------
INSERT INTO `galeria_tipo` VALUES (2, 'Mud Chiclayo', 'A');
INSERT INTO `galeria_tipo` VALUES (3, 'Mud Piura', 'I');
INSERT INTO `galeria_tipo` VALUES (4, 'Mud Trujillo', 'I');
INSERT INTO `galeria_tipo` VALUES (5, 'Mud Norte Peruano', 'A');
INSERT INTO `galeria_tipo` VALUES (6, 'Transporte de bienes ', 'A');
INSERT INTO `galeria_tipo` VALUES (7, 'Embalaje', 'A');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `codigo_menu` bigint(20) NOT NULL,
  `icono` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'fa-circle-o',
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `nivel` int(11) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 0,
  `archivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_padre` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo_menu`) USING BTREE,
  INDEX `id_padre`(`id_padre`) USING BTREE,
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'fa-circle-o', 'Privilegios', '', 'A', 1, 3, '', NULL);
INSERT INTO `menu` VALUES (2, 'fa-circle-o', 'Accesos', '', 'A', 2, 1, 'accesos', 1);
INSERT INTO `menu` VALUES (3, 'fa-circle-o', 'Mantenimiento', '', 'A', 1, 1, '', NULL);
INSERT INTO `menu` VALUES (4, 'fa-circle-o', 'Personal', '', 'A', 2, 1, 'personal', 3);
INSERT INTO `menu` VALUES (5, 'fa-circle-o', 'Tipo de Objeto', '', 'A', 2, 2, 'objeto_tipo', 3);
INSERT INTO `menu` VALUES (6, 'fa-circle-o', 'Objeto', '', 'A', 2, 3, 'objeto', 3);
INSERT INTO `menu` VALUES (7, 'fa-circle-o', 'Vehículo', '', 'A', 2, 4, 'vehiculo', 3);
INSERT INTO `menu` VALUES (8, 'fa-circle-o', 'Contáctanos', '', 'A', 1, 4, 'contacto', NULL);
INSERT INTO `menu` VALUES (9, 'fa-circle-o', 'Galería', '', 'A', 1, 2, '', NULL);
INSERT INTO `menu` VALUES (10, 'fa-circle-o', 'Categoría', '', 'A', 2, 1, 'galeria_categoria', 9);
INSERT INTO `menu` VALUES (11, 'fa-circle-o', 'Imagen', '', 'A', 2, 2, 'galeria_imagen', 9);

-- ----------------------------
-- Table structure for menu_accesos
-- ----------------------------
DROP TABLE IF EXISTS `menu_accesos`;
CREATE TABLE `menu_accesos`  (
  `menu` bigint(20) NOT NULL,
  `cargo` bigint(20) NOT NULL,
  `acceso` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `agregar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `modificar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `eliminar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `imprimir` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu`, `cargo`) USING BTREE,
  INDEX `rol`(`cargo`) USING BTREE,
  INDEX `menu`(`menu`) USING BTREE,
  CONSTRAINT `menu_accesos_ibfk_2` FOREIGN KEY (`menu`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_accesos_ibfk_3` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_accesos
-- ----------------------------
INSERT INTO `menu_accesos` VALUES (1, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (1, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (1, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 3, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 2, '0', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 3, '0', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for objeto
-- ----------------------------
DROP TABLE IF EXISTS `objeto`;
CREATE TABLE `objeto`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ancho` decimal(10, 2) NULL DEFAULT 0.00,
  `alto` decimal(10, 2) NULL DEFAULT 0.00,
  `largo` decimal(10, 2) NULL DEFAULT 0.00,
  `tipo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `tipo`(`tipo`) USING BTREE,
  CONSTRAINT `objeto_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `objeto_tipo` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objeto
-- ----------------------------
INSERT INTO `objeto` VALUES (1, 'refrigerador 1 puerta', '1.jpg', 2.00, 0.50, 0.50, 1, 'A');
INSERT INTO `objeto` VALUES (2, 'frigobar', '2.jpg', 0.47, 0.85, 0.45, 2, 'A');
INSERT INTO `objeto` VALUES (3, 'Frigobar', '3.jpg', 0.47, 0.85, 0.45, 3, 'A');
INSERT INTO `objeto` VALUES (4, 'Refrigeradora', '4.jpg', 0.70, 1.68, 0.73, 3, 'A');
INSERT INTO `objeto` VALUES (5, 'Refrigeradora 2 puertas', '5.jpg', 0.92, 1.79, 0.74, 3, 'A');
INSERT INTO `objeto` VALUES (6, 'Lavadora 12 KG', '6.jpg', 0.60, 0.97, 0.63, 4, 'A');
INSERT INTO `objeto` VALUES (7, 'Lavadora 20 KG', '7.jpg', 0.68, 1.08, 0.68, 4, 'A');
INSERT INTO `objeto` VALUES (8, 'Portatil', '8.jpg', 0.53, 0.12, 0.50, 5, 'A');
INSERT INTO `objeto` VALUES (9, '4 hornillas', '9.jpg', 0.51, 0.95, 0.63, 5, 'A');
INSERT INTO `objeto` VALUES (10, '6 hornillas', '10.jpg', 0.80, 0.94, 0.58, 5, 'A');
INSERT INTO `objeto` VALUES (11, 'Colchón 1.5 plazas', '11.jpg', 1.05, 0.20, 1.90, 6, 'A');
INSERT INTO `objeto` VALUES (12, 'Colchón 2 plazas', '12.jpg', 1.35, 0.30, 1.90, 6, 'A');
INSERT INTO `objeto` VALUES (13, 'Colchón Queen', '13.png', 1.53, 0.35, 2.03, 6, 'A');
INSERT INTO `objeto` VALUES (14, 'Colchón King', '14.png', 1.98, 0.36, 2.03, 6, 'A');
INSERT INTO `objeto` VALUES (15, 'Mesa 4 sillas', '15.jpg', 1.20, 0.78, 0.80, 7, 'A');
INSERT INTO `objeto` VALUES (16, 'Mesa 6 sillas', '16.png', 1.60, 0.78, 0.80, 7, 'A');
INSERT INTO `objeto` VALUES (17, 'Mesa 8 sillas', '17.png', 1.80, 0.78, 1.00, 7, 'A');
INSERT INTO `objeto` VALUES (18, 'silla', '18.jpg', 0.42, 1.03, 0.54, 8, 'A');
INSERT INTO `objeto` VALUES (19, 'TV caja', '19.jpg', 0.65, 0.50, 0.65, 9, 'A');
INSERT INTO `objeto` VALUES (20, 'TV 32\"', '20.jpg', 0.71, 0.40, 0.20, 9, 'A');
INSERT INTO `objeto` VALUES (21, 'TV 52\"', '21.jpg', 1.15, 0.65, 0.28, 9, 'A');

-- ----------------------------
-- Table structure for objeto_tipo
-- ----------------------------
DROP TABLE IF EXISTS `objeto_tipo`;
CREATE TABLE `objeto_tipo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objeto_tipo
-- ----------------------------
INSERT INTO `objeto_tipo` VALUES (1, 'Título de prueba', 'asdas', 'I');
INSERT INTO `objeto_tipo` VALUES (2, 'Artefactos eléctricos ', 'Artefactos eléctricos ', 'I');
INSERT INTO `objeto_tipo` VALUES (3, 'Refrigeradoras', 'Electrodomésticos de cocina', 'A');
INSERT INTO `objeto_tipo` VALUES (4, 'Lavadoras', 'Electrodomésticos de patio', 'A');
INSERT INTO `objeto_tipo` VALUES (5, 'Cocinas', 'Electrodomésticos de cocina', 'A');
INSERT INTO `objeto_tipo` VALUES (6, 'Colchones', 'Muebles de Cuarto', 'A');
INSERT INTO `objeto_tipo` VALUES (7, 'Mesas', 'Muebles de comedor', 'A');
INSERT INTO `objeto_tipo` VALUES (8, 'Sillas', 'Muebles de Comedor', 'A');
INSERT INTO `objeto_tipo` VALUES (9, 'TV', 'Electrodomésticos ', 'A');
INSERT INTO `objeto_tipo` VALUES (10, ' Juego de Sofá 3-2-1', 'Muebles de sentarse', 'A');
INSERT INTO `objeto_tipo` VALUES (11, 'Centro de entretenimiento', 'Mueble', 'A');
INSERT INTO `objeto_tipo` VALUES (12, 'Ropero', 'Mueble de madera o melamina', 'A');
INSERT INTO `objeto_tipo` VALUES (13, 'Cómoda', 'mueble ', 'A');
INSERT INTO `objeto_tipo` VALUES (14, 'Tarima', 'Mueble de habitación', 'A');
INSERT INTO `objeto_tipo` VALUES (15, 'Velador', 'Mueble de habitación', 'A');
INSERT INTO `objeto_tipo` VALUES (16, 'Sofá cama', 'mueble', 'A');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `codigo` bigint(20) NOT NULL,
  `dni` char(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_paterno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_materno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sexo` tinyint(4) NOT NULL,
  `telefono` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cargo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  INDEX `cargo`(`cargo`) USING BTREE,
  CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, '70269322', 'Admin', 'Admin', 'Admin', 0, '924-497-770', 'admin@gmail.com', 1, 'A');
INSERT INTO `persona` VALUES (2, '22222222', 'jose diego', 'vasquez', 'fernandez', 0, '997527651', 'josediego@gmail.com', 3, 'A');
INSERT INTO `persona` VALUES (3, '11111111', 'lia', 'santisteban', 'valderrama', 0, '953384901', 'lia@gmail.com', 2, 'A');
INSERT INTO `persona` VALUES (4, '12341234', 'mvilchez', 'mvilchez', 'mvilchez', 1, '12341234', 'mvilchez@gmail.com', 1, 'A');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `codigo` bigint(20) NOT NULL,
  `persona` bigint(20) NOT NULL,
  `contrasena` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `foto` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `sesiones` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `personal`(`persona`) USING BTREE,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`persona`) REFERENCES `persona` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 1, '202cb962ac59075b964b07152d234b70', 'A', 'default.jpg', 216);
INSERT INTO `usuario` VALUES (2, 2, 'bae5e3208a3c700e3db642b6631e95b9', 'A', 'default.jpg', 3);
INSERT INTO `usuario` VALUES (3, 3, '1bbd886460827015e5d605ed44252251', 'A', 'default.jpg', 4);
INSERT INTO `usuario` VALUES (4, 4, 'ed2b1f468c5f915f3f1cf75d7068baae', 'A', 'default.jpg', 1);

-- ----------------------------
-- Table structure for vehiculo
-- ----------------------------
DROP TABLE IF EXISTS `vehiculo`;
CREATE TABLE `vehiculo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `placa` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ancho` decimal(10, 2) NULL DEFAULT 0.00,
  `alto` decimal(10, 2) NULL DEFAULT 0.00,
  `largo` decimal(10, 2) NULL DEFAULT 0.00,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of vehiculo
-- ----------------------------
INSERT INTO `vehiculo` VALUES (1, 'Camión 1', '1.png', 'M5K-819', 2.00, 2.50, 2.50, 'A');
INSERT INTO `vehiculo` VALUES (2, 'Camión 2', '2.png', 'M1L-333', 2.50, 3.00, 5.00, 'A');
INSERT INTO `vehiculo` VALUES (3, 'Camión 3', '3.png', 'M1L-333', 3.00, 3.50, 8.50, 'A');

SET FOREIGN_KEY_CHECKS = 1;

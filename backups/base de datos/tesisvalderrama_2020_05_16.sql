/*
 Navicat Premium Data Transfer

 Source Server         : Localhost - MySQL
 Source Server Type    : MariaDB
 Source Server Version : 100122
 Source Host           : localhost:3306
 Source Schema         : tesisvalderrama

 Target Server Type    : MariaDB
 Target Server Version : 100122
 File Encoding         : 65001

 Date: 16/05/2020 10:39:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo`  (
  `codigo` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cargo
-- ----------------------------
INSERT INTO `cargo` VALUES (1, 'Administrador', 'A');
INSERT INTO `cargo` VALUES (2, 'Personal', 'A');
INSERT INTO `cargo` VALUES (3, 'Cliente', 'A');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `codigo` bigint(20) NOT NULL,
  `dni` char(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_paterno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_materno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sexo` tinyint(4) NOT NULL,
  `telefono` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cargo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  INDEX `cargo`(`cargo`) USING BTREE,
  CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, '70269322', 'Administrador', '', '', 0, '924-497-770', 'admin@gmail.com', 1, 'A');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `codigo` bigint(20) NOT NULL,
  `persona` bigint(20) NOT NULL,
  `contrasena` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `foto` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `sesiones` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `personal`(`persona`) USING BTREE,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`persona`) REFERENCES `persona` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 1, '202cb962ac59075b964b07152d234b70', 'A', 'default.jpg', 148);

SET FOREIGN_KEY_CHECKS = 1;

/*
 Navicat Premium Data Transfer

 Source Server         : Localhost - MySQL
 Source Server Type    : MariaDB
 Source Server Version : 100122
 Source Host           : localhost:3306
 Source Schema         : tesisvalderrama

 Target Server Type    : MariaDB
 Target Server Version : 100122
 File Encoding         : 65001

 Date: 23/05/2020 19:27:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo`  (
  `codigo` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cargo
-- ----------------------------
INSERT INTO `cargo` VALUES (1, 'Administrador', 'A');
INSERT INTO `cargo` VALUES (2, 'Cliente', 'A');
INSERT INTO `cargo` VALUES (3, 'Personal', 'A');

-- ----------------------------
-- Table structure for contacto
-- ----------------------------
DROP TABLE IF EXISTS `contacto`;
CREATE TABLE `contacto`  (
  `codigo` bigint(20) NOT NULL,
  `nombres` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `apellidos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `asunto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mensaje` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacto
-- ----------------------------
INSERT INTO `contacto` VALUES (1, 'Maykol', 'Maykol', 'maykol@gmail.com', 'algo pe ya tu sa', 'una consultita bro', 'A');

-- ----------------------------
-- Table structure for galeria
-- ----------------------------
DROP TABLE IF EXISTS `galeria`;
CREATE TABLE `galeria`  (
  `codigo` bigint(20) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tipo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `galeria_ibfk_1`(`tipo`) USING BTREE,
  CONSTRAINT `galeria_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `galeria_tipo` (`codigo`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of galeria
-- ----------------------------
INSERT INTO `galeria` VALUES (1, 'Una procesadora', 'esta es mi nueva procesadora de alimentos', '1.jpg', 1, 'A');

-- ----------------------------
-- Table structure for galeria_tipo
-- ----------------------------
DROP TABLE IF EXISTS `galeria_tipo`;
CREATE TABLE `galeria_tipo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of galeria_tipo
-- ----------------------------
INSERT INTO `galeria_tipo` VALUES (1, 'Probando', 'A');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `codigo_menu` bigint(20) NOT NULL,
  `icono` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'fa-circle-o',
  `nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `nivel` int(11) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL DEFAULT 0,
  `archivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_padre` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo_menu`) USING BTREE,
  INDEX `id_padre`(`id_padre`) USING BTREE,
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'fa-circle-o', 'Privilegios', '', 'A', 1, 3, '', NULL);
INSERT INTO `menu` VALUES (2, 'fa-circle-o', 'Accesos', '', 'A', 2, 1, 'accesos', 1);
INSERT INTO `menu` VALUES (3, 'fa-circle-o', 'Mantenimiento', '', 'A', 1, 1, '', NULL);
INSERT INTO `menu` VALUES (4, 'fa-circle-o', 'Personal', '', 'A', 2, 1, 'personal', 3);
INSERT INTO `menu` VALUES (5, 'fa-circle-o', 'Tipo de Objeto', '', 'A', 2, 2, 'objeto_tipo', 3);
INSERT INTO `menu` VALUES (6, 'fa-circle-o', 'Objeto', '', 'A', 2, 3, 'objeto', 3);
INSERT INTO `menu` VALUES (7, 'fa-circle-o', 'Vehículo', '', 'A', 2, 4, 'vehiculo', 3);
INSERT INTO `menu` VALUES (8, 'fa-circle-o', 'Contáctanos', '', 'A', 1, 4, 'contacto', NULL);
INSERT INTO `menu` VALUES (9, 'fa-circle-o', 'Galería', '', 'A', 1, 2, '', NULL);
INSERT INTO `menu` VALUES (10, 'fa-circle-o', 'Categoría', '', 'A', 2, 1, 'galeria_categoria', 9);
INSERT INTO `menu` VALUES (11, 'fa-circle-o', 'Imagen', '', 'A', 2, 2, 'galeria_imagen', 9);

-- ----------------------------
-- Table structure for menu_accesos
-- ----------------------------
DROP TABLE IF EXISTS `menu_accesos`;
CREATE TABLE `menu_accesos`  (
  `menu` bigint(20) NOT NULL,
  `cargo` bigint(20) NOT NULL,
  `acceso` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `agregar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `modificar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `eliminar` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `imprimir` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu`, `cargo`) USING BTREE,
  INDEX `rol`(`cargo`) USING BTREE,
  INDEX `menu`(`menu`) USING BTREE,
  CONSTRAINT `menu_accesos_ibfk_2` FOREIGN KEY (`menu`) REFERENCES `menu` (`codigo_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_accesos_ibfk_3` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_accesos
-- ----------------------------
INSERT INTO `menu_accesos` VALUES (1, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (1, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (1, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (2, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (3, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (4, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (5, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (6, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (7, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (8, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (9, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (10, 3, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 1, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 2, '1', '1', '1', '1', '1');
INSERT INTO `menu_accesos` VALUES (11, 3, '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for objeto
-- ----------------------------
DROP TABLE IF EXISTS `objeto`;
CREATE TABLE `objeto`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ancho` decimal(10, 2) NULL DEFAULT 0.00,
  `alto` decimal(10, 2) NULL DEFAULT 0.00,
  `largo` decimal(10, 2) NULL DEFAULT 0.00,
  `tipo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `tipo`(`tipo`) USING BTREE,
  CONSTRAINT `objeto_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `objeto_tipo` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objeto
-- ----------------------------
INSERT INTO `objeto` VALUES (1, 'refrigerador 1 puerta', '1.jpg', 2.00, 0.50, 0.50, 1, 'A');

-- ----------------------------
-- Table structure for objeto_tipo
-- ----------------------------
DROP TABLE IF EXISTS `objeto_tipo`;
CREATE TABLE `objeto_tipo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objeto_tipo
-- ----------------------------
INSERT INTO `objeto_tipo` VALUES (1, 'Título de prueba', 'asdas', 'A');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `codigo` bigint(20) NOT NULL,
  `dni` char(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_paterno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_materno` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sexo` tinyint(4) NOT NULL,
  `telefono` char(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cargo` bigint(20) NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  INDEX `cargo`(`cargo`) USING BTREE,
  CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, '70269322', 'Admin', 'Admin', 'Admin', 0, '924-497-770', 'admin@gmail.com', 1, 'A');

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `codigo` bigint(20) NOT NULL,
  `persona` bigint(20) NOT NULL,
  `contrasena` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `foto` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `sesiones` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`codigo`) USING BTREE,
  INDEX `personal`(`persona`) USING BTREE,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`persona`) REFERENCES `persona` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 1, '202cb962ac59075b964b07152d234b70', 'A', 'default.jpg', 163);

-- ----------------------------
-- Table structure for vehiculo
-- ----------------------------
DROP TABLE IF EXISTS `vehiculo`;
CREATE TABLE `vehiculo`  (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `placa` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ancho` decimal(10, 2) NULL DEFAULT 0.00,
  `alto` decimal(10, 2) NULL DEFAULT 0.00,
  `largo` decimal(10, 2) NULL DEFAULT 0.00,
  `estado` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'A',
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

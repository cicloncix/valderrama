<?php

require_once '../modelo/simulador.php';
require_once '../util/funciones/Funciones.clase.php';

try {
    
    // if ( !isset( $_POST["codigoCategoria"] )){
    //     Funciones::imprimeJSON(500, "Faltan parametros", "");
    //     exit;
    // }

    $_POST = json_decode(file_get_contents("php://input"),true);

    $codigo = $_POST["codigo"];

    
    $objCliente = new simulador();
    $resultado = $objCliente->producto($codigo);
    
    Funciones::imprimeJSON(200, "", $resultado);
    
    
} catch (Exception $exc) {
    //Funciones::mensaje($exc->getMessage(), "e");
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}

<?php

require_once '../modelo/simulador.php';
require_once '../util/funciones/Funciones.clase.php';

try {

    $_POST = json_decode(file_get_contents("php://input"),true);

    $nombres = $_POST["nombres"];
    $apellidos = $_POST["apellidos"];
    $telefono = $_POST["telefono"];
    $correo = $_POST["correo"];
    $direccion = $_POST["direccion"];
    $referencia = $_POST["referencia"];

    $mudanza = $_POST["mudanza"];
    $tipo = $_POST["tipo"];
    $origen = $_POST["origen"];
    $destino = $_POST["destino"];
    $fecha = $_POST["fecha"];
    $hora = $_POST["hora"];
    $pisoa = $_POST["pisoa"];
    $pisob = $_POST["pisob"];
    $precio = $_POST["precio"];
    $productosArray = $_POST["productos"];

    
    $objCliente = new simulador();
    $resultado = $objCliente->simulador_transaccion($nombres,$apellidos,$telefono,$correo,$direccion,$referencia,$mudanza,$tipo,$origen,$destino,$fecha,$hora,$pisoa,$pisob,$precio,$productosArray);
    
    Funciones::imprimeJSON(200, "", $resultado);
    
    
} catch (Exception $exc) {
    //Funciones::mensaje($exc->getMessage(), "e");
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}

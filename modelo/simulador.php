<?php

require_once '../datos/Conexion.clase.php';

class simulador extends Conexion
{
  public function tipo() {
        try {
            // $sql = "select * from objeto_tipo where estado like 'A' order by nombre asc";
            $sql = "select * from objeto_tipo order by nombre asc";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            
            return $resultado;
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }

     public function producto($codigo) {
        try {
            // $sql = "select * from objeto where tipo = :p_codigo and estado like 'A'";
            $sql = "select * from objeto where tipo = :p_codigo";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigo", $codigo);
            $sentencia->execute();
            
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            
            return $resultado;
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function vehiculo() {
        try {
            // $sql = "select * from objeto_tipo where estado like 'A' order by nombre asc";
            $sql = "select * from vehiculo order by codigo asc";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            
            return $resultado;
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function simulador_transaccion($nombres,$apellidos,$telefono,$correo,$direccion,$referencia,$mudanza,$tipo,$origen,$destino,$fecha,$hora,$pisoa,$pisob,$precio,$productosArray){
        try {
            
            $sql = "select (codigo + 1) as codigo from contacto order by 1 desc limit 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            $codigo = $resultado["codigo"];


            $sql = "INSERT INTO contacto( codigo, nombres, apellidos, telefono, email, asunto, mensaje)
                                 VALUES (:p_codigo, :p_nombres, :p_apellidos, :p_telefono, :p_email, 'simulador', '');";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigo", $codigo);
            $sentencia->bindParam(":p_nombres", $nombres);
            $sentencia->bindParam(":p_apellidos", $apellidos);
            $sentencia->bindParam(":p_telefono", $telefono);
            $sentencia->bindParam(":p_email", $correo);
            $sentencia->execute();

            /* */

            $sql = "select (id_simulador + 1) as id_simulador from simulador order by 1 desc limit 1";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            $id_simulador = $resultado["id_simulador"];
            
            $sql = "INSERT INTO simulador( id_simulador, codigo, origen, destino, fecha, hora, pisoa, pisob, precio, tipo_domicilio, tipo_mudanza, direccion, referencia)
                                 VALUES (:p_id_simulador, :p_codigo, :p_origen, :p_destino, :p_fecha, :p_hora, :p_pisoa, :p_pisob, :p_precio, :p_tipo_domicilio, :p_tipo_mudanza, :p_direccion, :p_referencia);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_simulador", $id_simulador);
            $sentencia->bindParam(":p_codigo", $codigo);
            $sentencia->bindParam(":p_origen", $origen);
            $sentencia->bindParam(":p_destino", $destino);
            $sentencia->bindParam(":p_fecha", $fecha);
            $sentencia->bindParam(":p_hora", $hora);
            $sentencia->bindParam(":p_pisoa", $pisoa);
            $sentencia->bindParam(":p_pisob", $pisob);
            $sentencia->bindParam(":p_precio", $precio);
            $sentencia->bindParam(":p_tipo_domicilio", $tipo);
            $sentencia->bindParam(":p_tipo_mudanza", $mudanza);
            $sentencia->bindParam(":p_direccion", $direccion);
            $sentencia->bindParam(":p_referencia", $referencia);
            $sentencia->execute();

            $detalleVentaArray = json_decode($productosArray);
            $item = 0;

             foreach ($detalleVentaArray as $key => $value) {
                $item++;

                $sql = "INSERT INTO simulador_detalle( id_simulador, item, codigo, cantidad) VALUES (:p_id_simulador, :p_item, :p_codigo, :p_cantidad );";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_id_simulador", $id_simulador);
                $sentencia->bindParam(":p_item", $item);
                $sentencia->bindParam(":p_codigo", $value->codigo);
                $sentencia->bindParam(":p_cantidad", $value->cantidad);
                $sentencia->execute();
             }


            return true;
        } catch (Exception $th) {
            throw $th;
        }
        return false;
    }
}
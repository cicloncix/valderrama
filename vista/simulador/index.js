$(function () {
  tipo();
  menu();

  $("#datetimepicker5").datetimepicker({
    format: "DD.MM.YYYY",
  });

  $("#datetimepicker6").datetimepicker({
    format: "LT",
  });
});

let tipo = async () => {
  let html = ``;
  let { data } = await axios.post("../../controlador/tipo.php", {});

  data.datos.map((e) => {
    html += `<p class="product-item" onclick="productos('${e.codigo}')">${e.nombre}</p>`;
  });

  $("#divtipo").html(html);
};

let vehiculo = async (number) => {
  let { data } = await axios.post("../../controlador/vehiculo.php", {});

  return data.datos[number];
};

let productobusqueda;

let productos = async (codigo) => {
  let html = ``;
  let { data } = await axios.post("../../controlador/producto.php", { codigo });

  productobusqueda = data.datos;

  data.datos.map((e) => {
    html += `<div class="col-sm-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="../../img/${e.imagen}" style="width: 100px">
                            <h5 class="card-text">${e.nombre}</h5>
                            <div class="text-center">
                                <button class="btn btn-primary" style="background-color: #148496;" onclick="restar(${e.codigo})">-</button>
                                <input placeholder="0" value="0" style="width: 50px;" id="cantidad${e.codigo}">
                                <button class="btn btn-primary" style="background-color: #148496;" onclick="sumar(${e.codigo})">+</button>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary" style="background-color: #148496;" onclick="agregar('${e.codigo}')">Agregar</button>
                            </div>
                        </div>
                    </div>
                </div>`;
  });

  $("#divproducto").html(html);
};

let sumar = (codigo) => {
  let tag = `cantidad${codigo}`;
  let valor = $(`#${tag}`).val();
  let suma = Number(valor) + 1;
  $(`#${tag}`).val(suma);
};

let restar = (codigo) => {
  let tag = `cantidad${codigo}`;
  let valor = $(`#${tag}`).val();
  let resta = Number(valor) - 1;
  if (resta >= 0) {
    $(`#${tag}`).val(resta);
  }
};

let hogar = document.querySelector(".mudanza.hogar");
let edificio = document.querySelector(".mudanza.edificio");
let corporativo = document.querySelector(".mudanza.corporativo");

hogar.onclick = () => {
  hogar.style.background = "#148496";
  edificio.style.background = "none";
  corporativo.style.background = "none";
};

edificio.onclick = () => {
  hogar.style.background = "none";
  edificio.style.background = "#148496";
  corporativo.style.background = "none";
};

corporativo.onclick = () => {
  hogar.style.background = "none";
  edificio.style.background = "none";
  corporativo.style.background = "#148496";
  location.href = "https://ciclon.vortipro.com/?contacto";
};

let productosArray = [];

let agregar = (codigo) => {
  let producto = productobusqueda.filter((dato) => dato.codigo === codigo)[0];

  let tag = `cantidad${producto.codigo}`;
  let cantidad = $(`#${tag}`).val();

  let prodbus = productosArray.filter(
    (dato) => dato.codigo === producto.codigo
  );

  if (cantidad > 0 && prodbus.length === 0) {
    productosArray.push({
      codigo: producto.codigo,
      nombre: producto.nombre,
      imagen: producto.imagen,
      alto: producto.alto,
      espacio: producto.alto <= 0.6 ? 1 : producto.alto <= 1.2 ? 2 : 3,
      cantidad,
    });

    menu();

    $("#tablaproductodetalle").html("");

    productodetalle(productosArray);
  }
};

let productodetalle = (data) => {
  let html = ``;

  data.map((dato) => {
    html += `
    <tr>
      <td>${dato.nombre}</td>
      <td>${dato.cantidad}</td>
      <td>
        <button onclick="eliminar(${dato.codigo})" style="border: none"><i class="fas fa-times-circle" style="color: #e59285"></i></button>
      </td>
    </tr>
    `;
  });

  $("#tablaproductodetalle").html(html);
};

let eliminar = (codigo) => {
  productosArray = productosArray.filter((dato) => dato.codigo != codigo);
  $("#tablaproductodetalle").html("");
  productodetalle(productosArray);

  menu();
};

let menu = () => {
  if (productosArray.length != 0) {
    document.getElementById("block1").style.display = "block";
    document.getElementById("block2").style.display = "block";
  } else {
    document.getElementById("block1").style.display = "none";
    document.getElementById("block2").style.display = "none";
  }
};

let ordenarsumar = (array) => {
  let producto = array.sort(
    (a, b) => parseInt(b.espacio) - parseInt(a.espacio)
  );

  let productos = [];
  let order = 0;
  producto.map((data) => {
    if (data.cantidad == 1) {
      order++;
      productos.push({
        order,
        codigo: data.codigo,
        nombre: data.nombre,
        alto: data.alto,
        espacio: data.espacio,
        imagen: data.imagen,
      });
    } else {
      for (let i = 0; i < data.cantidad; i++) {
        order++;
        productos.push({
          order,
          codigo: data.codigo,
          nombre: data.nombre,
          alto: data.alto,
          espacio: data.espacio,
          imagen: data.imagen,
        });
      }
    }
  });

  let cantidad = productos.length <= 24 ? 3 : 4;

  let copyproductos = productos;
  let final = [];
  productos.map((data) => {
    let busqueda2 = final.findIndex((obj) => {
      return obj.order == data.order;
    });
    if (busqueda2 == -1) {
      final.push({
        codigo: data.codigo,
        nombre: data.nombre,
        alto: data.alto,
        espacio: data.espacio,
        order: data.order,
        imagen: data.imagen,
      });

      copyproductos = copyproductos.filter((dato) => dato.order != data.order);

      if (data.espacio != cantidad) {
        let falta = cantidad - data.espacio;
        let busqueda =
          copyproductos.find((obj) => {
            return obj.espacio == falta;
          }) || -1;

        if (busqueda != -1) {
          final.push({
            codigo: busqueda.codigo,
            nombre: busqueda.nombre,
            alto: busqueda.alto,
            espacio: busqueda.espacio,
            order: busqueda.order,
            imagen: busqueda.imagen,
          });

          copyproductos = copyproductos.filter(
            (dato) => dato.order != busqueda.order
          );
        }
      }
    }
  });

  return final;
};

let procesar = async () => {
  let nombres = $("#txtnombres").val();
  let apellidos = $("#txtapellidos").val();
  let telefono = $("#txttelefono").val();
  let correo = $("#txtcorreo").val();
  let direccion = $("#txtdireccion").val();
  let referencia = $("#txtreferencia").val();

  // debugger;

  let mudanza =
    hogar.style.background == "rgb(20, 132, 150)"
      ? "h"
      : edificio.style.background == "rgb(20, 132, 150)"
      ? "e"
      : corporativo.style.background == "rgb(20, 132, 150)"
      ? "c"
      : "";

  let origen = $("#txtorigen").val();
  let destino = $("#txtdestino").val();
  let fecha = $("#datetimepicker5").val();
  let hora = $("#datetimepicker6").val();
  let pisoa = $("#txtpisoa").val();
  let pisob = $("#txtpisob").val();

  let tipo = $("input[name='optradio']:checked").val();

  let productos = ordenarsumar(productosArray);

  //BASICO
  //camion pequeño (70) + 2(10)(piso mayor)
  //camion mediano (100) + 2(10)(piso mayor)
  //camion grande (150) + 3(10)(piso mayor)

  //ORO
  //camion pequeño (70) + 2(10)(piso mayor) +30
  //camion mediano (100) + 2(10)(piso mayor) + 30
  //camion grande (150) + 3(10)(piso mayor) + 30

  //Diamante
  //camion pequeño (70) + 2(10)(piso mayor) + 5(objeto)
  //camion mediano (100) + 2(10)(piso mayor) + 5(objeto)
  //camion grande (150) + 3(10)(piso mayor) + 5(objeto)

  let piso = pisoa >= pisob ? pisoa : pisob;
  let camionprecio =
    productos.length <= 24 ? 70 : productos.length <= 80 ? 100 : 150;
  let personas = productos.length <= 80 ? 2 : 3;

  let precio = 0;

  if (tipo == "b") {
    precio = camionprecio + personas * 10 * piso;
  } else if (tipo == "o") {
    precio = camionprecio + personas * 10 * piso + 30;
  } else if (tipo == "d") {
    precio = camionprecio + personas * 10 * piso + 5 * productos.length;
  }

  let camion =
    productos.length <= 24
      ? await vehiculo(0)
      : productos.length <= 80
      ? await vehiculo(1)
      : await vehiculo(2);

  if (
    nombres.length != 0 &&
    apellidos.length != 0 &&
    telefono.length != 0 &&
    correo.length != 0 &&
    direccion.length != 0 &&
    referencia.length != 0 &&
    mudanza.length != 0 &&
    origen.length != 0 &&
    destino.length != 0 &&
    fecha.length != 0 &&
    hora.length != 0 &&
    pisoa.length != 0 &&
    pisob.length != 0
  ) {
    let { data } = await axios.post("../../controlador/simulador.php", {
      nombres,
      apellidos,
      telefono,
      correo,
      direccion,
      referencia,
      mudanza,
      tipo,
      origen,
      destino,
      fecha,
      hora,
      pisoa,
      pisob,
      precio,
      productos: JSON.stringify(productosArray),
    });

    if (data.estado == 200) {
      openWindowWithPost("http://localhost/vista/simulador/reporte2.php", {
        nombres,
        apellidos,
        telefono,
        correo,
        direccion,
        referencia,
        mudanza,
        tipo,
        origen,
        destino,
        fecha,
        hora,
        pisoa,
        pisob,
        precio,
        productoslista: JSON.stringify(productosArray),
        productos: JSON.stringify(productos),
        camion: JSON.stringify(camion),
      });
    }
  } else {
    alert("rellene los campos vacios");
  }
};

function openWindowWithPost(url, data) {
  let form = document.createElement("form");
  form.target = "_blank";
  form.method = "POST";
  form.action = url;
  form.style.display = "none";

  for (let key in data) {
    let input = document.createElement("input");
    input.type = "hidden";
    input.name = key;
    input.value = data[key];
    form.appendChild(input);
  }

  document.body.appendChild(form);
  form.submit();
  document.body.removeChild(form);
}

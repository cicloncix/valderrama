<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>simulador</title>
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <!-- datepicker -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

  <!-- axios -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  
  <!-- datatable -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/dataTables.bootstrap4.min.js" integrity="sha512-OQlawZneA7zzfI6B1n1tjUuo3C5mtYuAWpQdg+iI9mkDoo7iFzTqnQHf+K5ThOWNJ9AbXL4+ZDwH7ykySPQc+A==" crossorigin="anonymous"></script>

  <!-- fontawesome -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/js/all.min.js" integrity="sha512-M+hXwltZ3+0nFQJiVke7pqXY7VdtWW2jVG31zrml+eteTP7im25FdwtLhIBTWkaHRQyPrhO2uy8glLMHZzhFog==" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" integrity="sha512-xA6Hp6oezhjd6LiLZynuukm80f8BoZ3OpcEYaqKoCV3HKQDrYjDE1Gu8ocxgxoXmwmSzM4iqPvCsOkQNiu41GA==" crossorigin="anonymous" />


  <link rel="stylesheet" href="index.css">
</head>
<body>
  <h2 class="text-center"> <img src="https://ciclon.vortipro.com/recursos/imagenes/logo/logo2.png?x=6733" alt="" srcset="" style="width: 150px">  Distribucción<span style="color: #e59285;border-bottom: 1px solid #e59285"> de la mudanza</span> </h2>

  <div class="row" style="margin-top: 20px; margin-bottom: 20px">
    <div class="col-5">
        <h5 class="text-center">Datos <span style="color: #e59285;border-bottom: 1px solid #e59285">Personales</span></h5>
        <div class="centervh">
          <div class="form-space">
            <div class="form-group">
                <!-- <label for="txtnombres">Nombres: </label> -->
                <input type="text" class="form-control" id="txtnombres" placeholder="Nombres">
            </div>  
            <div class="form-group">
                <!-- <label for="txtapellidos">Apellidos: </label> -->
                <input type="text" class="form-control" id="txtapellidos" placeholder="Apellidos">
            </div> 
          </div>
          <div class="form-space">
            <div class="form-group">
                <!-- <label for="txttelefono">Telefono: </label> -->
                <input type="text" class="form-control" id="txttelefono" placeholder="Telefono">
            </div>  
            <div class="form-group">
                <!-- <label for="txtcorreo">Correo: </label> -->
                <input type="text" class="form-control" id="txtcorreo" placeholder="Correo">
            </div> 
          </div>
          <div class="form-space">
            <div class="form-group">
                <!-- <label for="txtdireccion">Dirección: </label> -->
                <input type="text" class="form-control" id="txtdireccion" placeholder="Dirección">
            </div>  
            <div class="form-group">
                <!-- <label for="txtreferencia">Referencia: </label> -->
                <input type="text" class="form-control" id="txtreferencia" placeholder="Referencia">
            </div> 
          </div>
        </div>
      </div>
    
    <div class="col-7">
      <h5 class="text-center" >Tipo <span style="color: #e59285;border-bottom: 1px solid #e59285">Domicilio</span></h5>
      <div class="form-space text-center" style="margin-top: 20px; margin-bottom: 20px">
        <div class="mudanza hogar">
          <div class="centervh">
            <img src="../../img/casa.png" style="height: 100px">
            <p style="margin: 0" >Mudanza Hogar</p>
          </div>
        </div>
        <div class="mudanza edificio">
          <div class="centervh">
            <img src="../../img/edificio.png" style="height: 100px">
            <p style="margin: 0" >Mudanza Departamento Residencia</p>
          </div>
        </div>
        <div class="mudanza corporativo">
          <div class="centervh">
            <img src="../../img/corporativo.png" style="height: 100px">
            <p style="margin: 0" >Mudanza Corporativo</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row" style="margin-top: 20px; margin-bottom: 20px">
    <div class="col-5">
      <h5 class="text-center" >Lugar <span style="color: #e59285;border-bottom: 1px solid #e59285">y Fecha</span></h5>
      <div class="centervh">
        <div class="form-space">
          <div class="form-group">
              <!-- <label for="txtorigen">Origen: </label> -->
              <input type="text" class="form-control" id="txtorigen" placeholder="Referencia">
          </div>  
          <div class="form-group">
              <!-- <label for="txtdestino">Destino: </label> -->
              <input type="text" class="form-control" id="txtdestino" placeholder="Destino">
          </div> 
        </div> 
        <div class="form-space">
          <div class="form-group">
              <!-- <label for="txtfecha">Fecha: </label> -->
              <input type="text" class="form-control datetimepicker-input" id="datetimepicker5" data-toggle="datetimepicker" data-target="#datetimepicker5"placeholder="Fecha"/>
          </div>  
          <div class="form-group">
              <!-- <label for="txthora">Hora: </label> -->
              <input type="text" class="form-control datetimepicker-input" id="datetimepicker6" data-toggle="datetimepicker" data-target="#datetimepicker6" placeholder="Hora"/>
          </div> 
        </div>
        <div class="form-space">
          <div class="form-group">
              <!-- <label for="txtpisoa">Piso A: </label> -->
              <input type="number" class="form-control" id="txtpisoa" placeholder="Piso A">
          </div>  
          <div class="form-group">
              <!-- <label for="txtpisob">Piso B: </label> -->
              <input type="number" class="form-control" id="txtpisob" placeholder="Piso B">
          </div> 
        </div> 
      </div>
    </div>
    <div class="col-7">
      <h5 class="text-center">Tipo <span style="color: #e59285;border-bottom: 1px solid #e59285">Mudanza</span></h5>
      <div class="form-group">
        <table class="table text-center">
        <thead style="background-color: black; color: white">
          <tr>
            <th scope="col"></th>
            <th scope="col">Permisos</th>
            <th scope="col">Embalaje</th>
            <th scope="col">Personal de mudanza</th>
            <th scope="col">Desembalaje</th>
            <th scope="col">Reubicación</th>
            <th scope="col">Transporte</th>
          </tr>
        </thead>
        <tbody>
        <tr>
        <td>
          <label><input type="radio" value="b" name="optradio" checked><br>Básico</label>
        </td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td></td>
        <td></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        </tr>
        <tr>
        <td>
          <label><input type="radio" value="o" name="optradio"><br>Oro</label>
        </td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        </tr>
        <tr>
        <td>
          <label><input type="radio" value="d" name="optradio"><br>Diamante</label>
        </td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        <td><i class="fas fa-times-circle" style="color: #e59285"></i></td>
        </tr>
        </tbody>
        </table>
      </div>
    </div>
  </div>

  <div style="margin-top: 20px; margin-bottom: 20px">
    <h5 class="text-center">Seleccionar <span style="color: #e59285;border-bottom: 1px solid #e59285">Productos</span></h5>
    <div class="row">
      <div class="col-4">
        <div class="product-container" id="divtipo"></div>
      </div>
      <div class="col-8">
        <div class="form-space" id="divproducto"></div>
      </div>
    </div>
  </div>

  <div class="" style="margin-top: 20px; margin-bottom: 20px" id="block1">
    <h5 class="text-center">Productos <span style="color: #e59285;border-bottom: 1px solid #e59285">Detalles</span></h5>
    <div id="divdetalle">
      <table class="table text-center">
        <thead style="background-color: black; color: white">
          <tr>
            <th scope="col">Producto</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Opciones</th>
          </tr>
        </thead>
        <tbody id="tablaproductodetalle">
        </tbody>
        </table>
    </div>
  </div>

  <div class="text-center" id="block2">
    <button onclick="procesar()" class="btn btn-primary" style="background-color: #148496;">Procesar proforma</button>
  </div>

  <script src="index.js"></script>
</body>
</html>
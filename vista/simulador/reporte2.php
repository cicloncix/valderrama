<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="reporte.css">
</head>
<body>
    <div style="width: 595px;margin: 0 auto;">
        <h1 class="text-center">Resumen <span style="color: #e59285;border-bottom: 1px solid #e59285">de mudanza</span></h1>
        <button onclick="print()" class="btn btn-primary noprint" style="background-color: #148496;position: absolute;right: 25%;">Imprimir</button>

        <div style="margin-top: 20px;">
            <h3 class="text-center">Datos  <span style="color: #e59285;border-bottom: 1px solid #e59285">de mudanza</span></h3>
            <div class="row">
                <div class="col-6">
                    <p><span style="font-weight: bold;">Nombres y Apellidos: </span><?php echo($_POST["nombres"].' '.$_POST["apellidos"]) ?></p>
                    <p><span style="font-weight: bold;">Telefono:  </span><?php echo($_POST["telefono"]) ?></p>
                    <p><span style="font-weight: bold;">Correo:  </span><?php echo($_POST["correo"]) ?></p>
                    <p><span style="font-weight: bold;">Dirección: </span><?php echo($_POST["direccion"]) ?></p>
                    <p><span style="font-weight: bold;">Referencia: </span><?php echo($_POST["referencia"]) ?></p>
                </div>
                <div class="col-6">
                    <p><span style="font-weight: bold;">Origen: </span><?php echo($_POST["origen"]) ?></p>
                    <p><span style="font-weight: bold;">Destino: </span><?php echo($_POST["destino"]) ?></p>
                    <p><span style="font-weight: bold;">Fecha y hora: </span><?php echo($_POST["fecha"].' - '.$_POST["hora"]) ?></p>
                    <p><span style="font-weight: bold;">Piso A: </span><?php echo($_POST["pisoa"]) ?>  - Piso B: <?php echo($_POST["pisob"]) ?></p>
                    <p><span style="font-weight: bold;">Precio: </span><?php echo($_POST["precio"]) ?></p>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;">
            <h3 class="text-center">Datos  <span style="color: #e59285;border-bottom: 1px solid #e59285">de transporte</span></h3>
            <div class="row">
                <div class="col-6">
                    <?php 
                        $camion = json_decode($_POST["camion"]);
                        echo("<img src='../../img/".$camion->imagen."' style='width: 150px'>")
                    ?>
                </div>
                <div class="col-6">
                    <?php 
                        echo("<p>Placa: ".$camion->placa."</>")
                    ?>
                </div>
            </div>
        </div>
        <div style="margin-top: 20px;">
            <h3 class="text-center">Objetos  <span style="color: #e59285;border-bottom: 1px solid #e59285">a transportar</span></h3>
            <div class="row" style="margin: 0">
                <div class="col-12" style="display: contents;">
                    <?php

                    $productoslista = json_decode($_POST["productoslista"]);
                    $listarproductos = "";

                    foreach ($productoslista as $key => $valor) {
                        $espacio = (int)3 - (int)$valor->espacio;
                        $listarproductos .= '<p style="margin: 0; padding: 0;margin-right: 10px">('.$valor->cantidad.') '.$valor->nombre.'</p>';
                    }

                    echo($listarproductos);
                    ?>
                    <!-- <p style="margin: 0; padding: 0;margin-right: 10px">Capacidad: xxxxxxxxxx</p> -->
                </div>
            </div>
        </div>

        <div style="margin-top: 20px; height: 100px">
            <h3 class="text-center">Orden  <span style="color: #e59285;border-bottom: 1px solid #e59285">de los objetos</span></h3>
        <?php 

        function cubo($id,$alto,$nombre,$imagen){
            if($alto<= 0.60){
                return "<div class='cube' data-toggle='tooltip' data-placement='top' title='".$nombre."' id='".$id."-".$imagen."'>
                                        <div class='face top'></div>
                                        <div class='face bottom'></div>
                                        <div class='face left'></div>
                                        <div class='face right'></div>
                                        <div class='face front'></div>
                                        <div class='face back'></div>
                                    </div>";
            }elseif($alto <= 1.20){
                return "<div class='cube2' data-toggle='tooltip' data-placement='top' title='".$nombre."' id='".$id."-".$imagen."'>
                                        <div class='face2 top2'></div>
                                        <div class='face2 bottom2'></div>
                                        <div class='face2 left2'></div>
                                        <div class='face2 right2'></div>
                                        <div class='face2 front2'></div>
                                        <div class='face2 back2'></div>
                                    </div>";
            }else{
                return "<div class='cube3' data-toggle='tooltip' data-placement='top' title='".$nombre."' id='".$id."-".$imagen."'>
                                        <div class='face3 top3'></div>
                                        <div class='face3 bottom3'></div>
                                        <div class='face3 left3'></div>
                                        <div class='face3 right3'></div>
                                        <div class='face3 front3'></div>
                                        <div class='face3 back3'></div>
                                    </div>";
            }
        }
        
        $productos = json_decode($_POST["productos"]);
        $cantidadproductos = count($productos);

        if($cantidadproductos<= 24){
            $html = "<div class='container3' style='position: relative;left: -100px;top: -30px;'>";
            $html2 = "<div class='container3' style='position: relative;left: -100px;top: -100px;'>";
            $cantidafinal = 0;
            foreach ($productos as $key => $valor) {
                $cantidafinal++;
                if($cantidafinal <= 12){
                    $html .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }else{
                    $html2 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }
            }
            $html .= "</div>";
            $html2 .= "</div>";

            echo($html);
            echo($html2);
        }elseif($cantidadproductos <= 80){
            $html = "<div class='container4' style='position: relative;left: -100px;top: -60px;'>";
            $html2 = "<div class='container4' style='position: relative;left: -100px;top: -170px;'>";
            $html3 = "<div class='container4' style='position: relative;left: -100px;top: -280px;'>";
            $html4 = "<div class='container4' style='position: relative;left: -100px;top: -390px;'>";
            $cantidafinal = 0;

            foreach ($productos as $key => $valor) {
                $cantidafinal++;
                if($cantidafinal <= 12){
                    $html .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }elseif($cantidafinal <= 24){
                    $html2 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }elseif($cantidafinal <= 36){
                    $html3 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }else{
                    $html4 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }
            }

            $html .= "</div>";
            $html2 .= "</div>";
            $html3 .= "</div>";
            $html4 .= "</div>";

            echo($html);
            echo($html2);
            echo($html3);
            echo($html4);
        }else{
            $html = "<div class='container4' style='position: relative;left: -100px;top: -60px;'>";
            $html2 = "<div class='container4' style='position: relative;left: -100px;top: -170px;'>";
            $html3 = "<div class='container4' style='position: relative;left: -100px;top: -280px;'>";
            $html4 = "<div class='container4' style='position: relative;left: -100px;top: -390px;'>";
            $cantidafinal = 0;

            foreach ($productos as $key => $valor) {
                $cantidafinal++;
                if($cantidafinal <= 15){
                    $html .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }elseif($cantidafinal <= 30){
                    $cantidafinal++;
                    $html2 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }elseif($cantidafinal <= 45){
                    $html3 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }else{
                    $html4 .= cubo($valor->order,$valor->alto,$valor->nombre,$valor->imagen);
                }
            }

            $html .= "</div>";
            $html2 .= "</div>";
            $html3 .= "</div>";
            $html4 .= "</div>";

            echo($html);
            echo($html2);
            echo($html3);
            echo($html4);
        }
        ?>
    </div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js" integrity="sha512-OqcrADJLG261FZjar4Z6c4CfLqd861A3yPNMb+vRQ2JwzFT49WT4lozrh3bcKxHxtDTgNiqgYbEUStzvZQRfgQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
    <script>

$('.cube').on('mouseover', function (e) {
    let id=(e.currentTarget.id).split("-")[0]
    let imagen=(e.currentTarget.id).split("-")[1]

    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = `url(http://localhost/img/${imagen})`;
    div.style.transform = "scale(5) rotateZ(90deg)"
    div.style.zIndex = "1"
     div.style.backgroundRepeat = "no-repeat"
     div.style.backgroundSize = "contain"
     div.style.backgroundPosition = "center"
     
     let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "transparent";
            nodes[i].style.border = "none";
        }
    }
});

$('.cube').on('mouseout', function (e) {
    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = ``;
    div.style.transform = "scale(1)"
    div.style.zIndex = "initial"

    let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "skyblue";
            nodes[i].style.border = "2px solid black";
        }
    }
});

$('.cube2').on('mouseover', function (e) {
    let id=(e.currentTarget.id).split("-")[0]
    let imagen=(e.currentTarget.id).split("-")[1]

    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = `url(http://localhost/img/${imagen})`;
    div.style.transform = "scale(5) rotateZ(90deg)"
    div.style.zIndex = "1"
     div.style.backgroundRepeat = "no-repeat"
     div.style.backgroundSize = "contain"
     div.style.backgroundPosition = "center"
     
     let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "transparent";
            nodes[i].style.border = "none";
        }
    }
});

$('.cube2').on('mouseout', function (e) {
    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = ``;
    div.style.transform = "scale(1)"
    div.style.zIndex = "initial"

    let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "skyblue";
            nodes[i].style.border = "2px solid black";
        }
    }
});

$('.cube3').on('mouseover', function (e) {
    let id=(e.currentTarget.id).split("-")[0]
    let imagen=(e.currentTarget.id).split("-")[1]

    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = `url(http://localhost/img/${imagen})`;
    div.style.transform = "scale(5) rotateZ(90deg)"
    div.style.zIndex = "1"
     div.style.backgroundRepeat = "no-repeat"
     div.style.backgroundSize = "contain"
     div.style.backgroundPosition = "center"
     
     let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "transparent";
            nodes[i].style.border = "none";
        }
    }
});

$('.cube3').on('mouseout', function (e) {
    let div = document.getElementById(`${e.currentTarget.id}`);
    div.style.backgroundImage = ``;
    div.style.transform = "scale(1)"
    div.style.zIndex = "initial"

    let nodes = div.childNodes
     for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div') {
            nodes[i].style.background = "skyblue";
            nodes[i].style.border = "2px solid black";
        }
    }
});
        </script>

</body>
</html>